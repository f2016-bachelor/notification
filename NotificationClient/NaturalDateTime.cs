﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotificationClient
{
    public static class NaturalDateTime
    {
        public static string ToPrittyString(this DateTime d)
        {
            return GetPrettyDate(DateTime.Now.Subtract(d), d);
        }

        public static string GetPrettyDate(TimeSpan s, DateTime d)
        {
            // 2.
            // Get total number of days elapsed.
            int dayDiff = (int)s.TotalDays;

            // 3.
            // Get total number of seconds elapsed.
            int secDiff = (int)s.TotalSeconds;

            // 4.
            // Don't allow out of range values.
            if (dayDiff < 0 || dayDiff >= 31)
            {
                return null;
            }

            // 5.
            // Handle same-day times.
            if (dayDiff == 0)
            {
                // A.
                // Less than one minute ago.
                if (secDiff < 60)
                {
                    return "lige nu";
                }
                // B.
                // Less than 2 minutes ago.
                if (secDiff < 120)
                {
                    return "for et minut siden";
                }
                // C.
                // Less than one hour ago.
                if (secDiff < 3600)
                {
                    return string.Format("for {0} minutter siden",
                        Math.Floor((double)secDiff / 60));
                }
                // D.
                // Less than 2 hours ago.
                if (secDiff < 7200)
                {
                    return "for en time siden";
                }
                // E.
                // Less than one day ago.
                if (secDiff < 86400)
                {
                    return string.Format("for {0} timer siden",
                        Math.Floor((double)secDiff / 3600));
                }
            }
            // 6.
            // Handle previous days.
            if (dayDiff == 1)
            {
                return string.Format("i går klokken {0:HH:mm}", d);
            }
            if (dayDiff < 7)
            {
                return string.Format("for {0} dage siden",
                dayDiff);
            }
            if (dayDiff < 7*2)
            {
                return string.Format("for en uge siden");
            }

            if (dayDiff < 31)
            {
                return string.Format("for {0} uger siden",
                Math.Ceiling((double)dayDiff / 7));
            }

            return null;
        }
    }
}
