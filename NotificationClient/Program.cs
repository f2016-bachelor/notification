﻿using CommonCore;
using CommonCore.DTO;
using Microsoft.Speech.Synthesis;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Web.Script.Serialization;

namespace NotificationClient
{
    public class NotificationService
    {
        private static readonly TraceSource Tracer = new TraceSource(typeof(NotificationService).FullName);
        private System.Timers.Timer aTimer;
        public Device devClass = new Device();
        private HttpClient Client = new HttpClient();
        public string token;
        public SpeechSynthesizer speech;
        private System.Media.SoundPlayer m_SoundPlayer;
        private NotificationSource notificationSource;

        private string url = "https://assignit.herokuapp.com/api/";

        public NotificationService()
        {
            url = Config.ReadSetting("Url") ?? url;
            notificationSource = new NotificationSource(url);
            speech = new SpeechSynthesizer();

            // Create a SoundPlayer instance to play the output audio file.
            m_SoundPlayer =
              new System.Media.SoundPlayer();

            foreach (var voice in speech.GetInstalledVoices())
            {
                if (voice.VoiceInfo.Culture == CultureInfo.CurrentCulture)
                {
                    speech.SelectVoice(voice.VoiceInfo.Name);
                    if (speech.Voice != voice.VoiceInfo)
                    {
                        Tracer.TraceEvent(TraceEventType.Error, 0, "Unable to set Voice");
                    }
                    speech.SetOutputToDefaultAudioDevice();

                    Tracer.TraceInformation("Using Voice: " + voice.VoiceInfo.Name + ", " + voice.VoiceInfo.Culture + ", " + voice.VoiceInfo.Description + ", " + voice.VoiceInfo.Gender);
                }
            }

            speech.Rate = Int16.Parse(Config.ReadSetting("Rate", "-3"));
            speech.TtsVolume = Int16.Parse(Config.ReadSetting("TtsVolume", "100"));
            speech.Volume = Int16.Parse(Config.ReadSetting("Volume", "100"));

            // setup timer
            aTimer = new System.Timers.Timer(5000); // 5sec. 5 minutes : 300000
            aTimer.Elapsed += GetNotifications; // Hook up the Elapsed event for the timer. GetNotifications called every time timer Elapsed.
            aTimer.AutoReset = false;
        }

        public void Start()
        {
            var t = LoginDevice(); // Login Device, to ensure notifications from one specific device. Sets Authorization Header token.
            aTimer.Start();
        }

        public void Continue()
        {
            aTimer.Start();
        }

        public void Stop()
        {
            aTimer.Stop();
        }

        /// <summary>
        /// Gets notifications from server.
        /// Adds token from login-process in header and calls Get to server.
        /// Called on Timer-tick
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private async void GetNotifications(Object source, ElapsedEventArgs e)
        {
            try
            {
                var assignments = await notificationSource.GetNotifications(token);

                // filter so they are not notified immediately
                assignments = assignments.FilterNotificationsOnDelay(TimeSpan.FromMinutes(5), TimeSpan.FromHours(2), TimeSpan.FromDays(1));

                StringBuilder b = new StringBuilder();
                b.AppendLine("*------------------------------------------*");

                var texts = assignments.Select((assignment) =>
                {                    // get data from object n, parsed to Assignment object
                    string name = assignment.name;
                    string date = assignment.date;

                    // parse date to better format, that ISO
                    var parseddate = DateTime.Parse(date);
                    var user = assignment.users[0];
                    string userString = user.firstName + " " + user.lastName;

                    // compose notification string and add to list
                    return string.Format("{0}: {1} skulle være udført {2}", userString, name, parseddate.ToPrittyString() ?? string.Format("{0:dd-MM-yyyy} klokken {0:HH:mm}", parseddate));
                }).ToList();

                texts.ForEach(text => b.AppendLine(text));

                b.AppendLine("*------------------------------------------*");

                Tracer.TraceInformation(b.ToString());
                Console.WriteLine(b);

                Speak(String.Join(". ", texts));
            }
            catch (Exception ex) // something gone wrong in request
            {
                Tracer.TraceEvent(TraceEventType.Warning, 0, ex.Message);
            }
            finally
            {
                aTimer.Start(); // start the timer again
            }
        }

        void Speak(string text)
        {
            m_SoundPlayer.Stream = new System.IO.MemoryStream();
            speech.SetOutputToWaveStream(m_SoundPlayer.Stream);

            speech.Speak(text);
            m_SoundPlayer.Stream.Position = 0;

            m_SoundPlayer.PlaySync();
        }

        /// <summary>
        /// Logs in device based on input from user
        /// If successful, the token generated by login-process is stored
        /// </summary>
        /// <returns></returns>
        public async Task LoginDevice()
        {
                token = Config.ReadSetting("Token");
                if (token == null)
                {
                    token = await notificationSource.LoginDevice(token);
                    Config.AddUpdateAppSettings("Token", token);
                }
        }
    }

    // api/notification
    class Program
    {
        public static void Main(string[] args)
        {
            var m = new NotificationService();
            m.Start();
            Console.ReadLine();
        }
    }
}
