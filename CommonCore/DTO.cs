﻿namespace CommonCore.DTO
{
    public class Device
    {
        public string id;
        public string password;
    }

    public class Token
    {
        public string token { get; set; }
    }

    public class Users
    {
        public string _id { get; set; }
        public string middleName { get; set; }
        public string lastName { get; set; }
        public string firstName { get; set; }
        public string __v { get; set; }
    }

    public class Assignment
    {
        public string id { get; set; }
        public string name { get; set; }
        public string priority { get; set; }
        public string date { get; set; }
        public Users[] users { get; set; }
        public string[] roles { get; set; }
        public bool userInputNeeded { get; set; }
        public bool recurring { get; set; }
        public string recurringInfo { get; set; }
        public string description { get; set; }
        public string userInput { get; set; }
        public bool finished { get; set; }
        public int seqno { get; set; }
        public bool alarm { get; set; }
    }
}
