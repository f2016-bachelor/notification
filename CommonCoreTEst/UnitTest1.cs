﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CommonCore.DTO;
using System.Collections.Generic;
using CommonCore;

namespace CommonCoreTEst
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            var list = new List<Assignment>();
            list.Add(new Assignment { priority = "High", date = DateTime.Now.AddMinutes(-10).ToString("o") });
            list.FilterNotificationsOnDelay(TimeSpan.FromMinutes(1));

            Assert.AreEqual(1, list.Count);
        }
    }
}
