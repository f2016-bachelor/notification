﻿using CommonCore;
using CommonCore.DTO;
using System;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Web.Script.Serialization;

namespace NotificationSenderService
{
    internal class ToSend
    {
        public object value1;
        public object value2;
        public object value3;
    }

    public class NotificationService
    {
        private static readonly TraceSource Tracer = new TraceSource(typeof(NotificationService).FullName);
        private System.Timers.Timer aTimer;
        public Device devClass = new Device();
        private HttpClient Client = new HttpClient();
        public string token;
        private NotificationSource notificationSource;
        private JavaScriptSerializer serializer = new JavaScriptSerializer();
        private Subject<Tuple<Assignment, string>> queue = new Subject<Tuple<Assignment, string>>();

        private string url = "https://assignit.herokuapp.com/api/";
        private string MakerEventURI = "https://maker.ifttt.com/trigger/assignment_overdue/with/key/co8z8G9Vk2ZEEfWdywFebC";

        public NotificationService()
        {
            url = Config.ReadSetting("Url") ?? url;
            MakerEventURI = Config.ReadSetting("MakerUri") ?? MakerEventURI;
            notificationSource = new NotificationSource(url);

            // setup timer
            aTimer = new System.Timers.Timer(TimeSpan.FromSeconds(10).TotalMilliseconds); // 5sec. 5 minutes : 300000
            aTimer.Elapsed += GetNotifications; // Hook up the Elapsed event for the timer. GetNotifications called every time timer Elapsed.
            aTimer.AutoReset = false;

            // Assignments gets filtered where assignment.id and assignment.date is used as a combined key, it is dropped from the queue if a notifikation have already been sent.
            var distinctAssignmentsOnly = queue.Distinct((assignment) => assignment.Item1.id + assignment.Item1.date);

            var sendTasks = distinctAssignmentsOnly.Select((tuple) =>
            {
                var httpRequest = new HttpRequestMessage(HttpMethod.Post, MakerEventURI);
                var postPayload = new ToSend { value1 = tuple.Item2, value2 = tuple.Item1 };
                httpRequest.Content = new StringContent(serializer.Serialize(postPayload), Encoding.UTF8, "application/json");
                return Client.SendAsync(httpRequest);
            });

            sendTasks
                .Where((task) => task.IsFaulted)
                .Subscribe((task) => Tracer.TraceEvent(TraceEventType.Warning, 0, task.Exception.Message));
        }

        public void Start()
        {
            var t = LoginDevice(); // Login Device, to ensure notifications from one specific device. Sets Authorization Header token.
            aTimer.Start();
        }

        public void Continue()
        {
            aTimer.Start();
        }

        public void Stop()
        {
            aTimer.Stop();
        }

        public TimeSpan PriorityToDelay(string priority)
        {
            switch (priority)
            {
                case "High": return TimeSpan.FromMinutes(10);
                case "Normal": return TimeSpan.FromHours(1);
                default:
                case "Low": return TimeSpan.FromDays(1);
            }
        }

        /// <summary>
        /// Gets notifications from server.
        /// Adds token from login-process in header and calls Get to server.
        /// Called on Timer-tick
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private async void GetNotifications(Object source, ElapsedEventArgs e)
        {
            try
            {
                var assignments = (await notificationSource.GetNotifications(token)).ToList();

                var filteredAssignments = assignments.FilterNotificationsOnDelay(TimeSpan.FromMinutes(10), TimeSpan.FromHours(4));

                // filter assignment based on wanted delay before notifying care personnel
                var messagesToSend = filteredAssignments.Select((assignment) =>
                {                    // get data from object n, parsed to Assignment object
                    string name = assignment.name;
                    string date = assignment.date;

                    // parse date to better format, that ISO
                    var parseddate = DateTime.Parse(date);
                    var user = assignment.users[0];
                    string userString = user.firstName + " " + user.lastName;

                    // compose notification string and add to list
                    return Tuple.Create(assignment, string.Format("{0}: {1} skulle være udført {2:dd-MM-yyyy} klokken {2:HH:mm}", userString, name, parseddate));
                });

                foreach (var assignment in messagesToSend)
                {
                    queue.OnNext(assignment);
                }

                StringBuilder b = new StringBuilder();
                b.AppendLine("*------------------------------------------*");

                foreach (var t in messagesToSend)
                {
                    b.AppendLine(t.Item2);
                }
                b.AppendLine("*------------------------------------------*");

                Tracer.TraceInformation(b.ToString());
                Console.WriteLine(b);
            }
            catch (Exception ex) // something gone wrong in request
            {
                Tracer.TraceEvent(TraceEventType.Warning, 0, ex.Message);
            }
            finally
            {
                aTimer.Start();
            }
        }

        /// <summary>
        /// Logs in device based on input from user
        /// If successful, the token generated by login-process is stored
        /// </summary>
        /// <returns></returns>
        public async Task LoginDevice()
        {
            token = Config.ReadSetting("Token");
            if (token == null)
            {
                token = await notificationSource.LoginDevice(token);
                Config.AddUpdateAppSettings("Token", token);
            }
        }
    }

    // api/notification
    internal class Program
    {
        public static void Main(string[] args)
        {
            var m = new NotificationService();
            m.Start();
            Console.ReadLine();
        }
    }
}
